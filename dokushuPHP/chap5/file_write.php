<?php
//アクセスログ情報をテキストファイルに書き込み
//書き込み内容を配列$dataにセット
$data [] = date('Y/m/d H:i:s');
$data [] = $_SERVER['SCRIPT_NAME'];
$data [] = $_SERVER['HTTP_USER_AGENT'];
$data [] = $_SERVER['HTTP_REFERER'];
print_r($data); echo "<br>";//配列の中身を表示
//アクセスログを追記書き込みモードでオープン
$file = fopen('access.log','ab') or die("ファイルを開くの失敗<br>");//or演算子のショートカットを利用
//ファイルのロック
flock($file,LOCK_EX);
//ファイルの書き込み
fwrite($file, implode("\n",$data)."\n");
//ファイルのクローズとロックの解除
fclose($file);
print "アクセスログを記録しました";