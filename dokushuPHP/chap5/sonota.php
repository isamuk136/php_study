<?php
//数学の関数
print abs(-334)."<br>"; //結果:334
print base_convert(1111, 2, 10)."<br>"; //結果:15
print ceil(9799.334)."<br>"; //結果:9800
print floor(9800.334)."<br>"; //結果:9800
print fmod(114.514,334)."<br>"; //結果:114.514
print max(334,9800,114514)."<br>"; //結果:114514
print min(334,9800,114514)."<br>"; //結果:334
print mt_rand(1,10)."<br>"; //結果:（１から１０の範囲でランダムに変わる）
print pow(2,7)."<br>"; //結果:128
print round(334.9800114514,4)."<br>"; //結果:334.98
print sqrt(144)."<hr>"; //結果:12
//その他、三角関数などもある
//unset関数（変数を破棄）
$str;
//var_dump($str); echo "<br>";//結果：エラー
$str="変数に値をセット";
var_dump($str); echo "<br>";//結果：string(24) "変数に値をセット" 
unset($str);
//var_damp($str); //結果：エラー
echo "<hr>";
//変数のデータ型を判断する
var_dump(is_int(334)); echo "<br>";//結果：bool(true)
var_dump(is_int('334')); echo "<br>";//結果：bool(false)
var_dump(is_int('334')); echo "<br>";//結果：bool(false)
var_dump(is_numeric('334')); echo "<br>";//結果：bool(true)
var_dump(is_float(334.114514)); echo "<br>";//結果：bool(true)
var_dump(is_resource(fopen('access.log','rb'))); echo "<br>";//結果：bool(true)
var_dump(is_null('')); echo "<hr>";//結果：bool(false)