<?php
//練習１
$str = '住所は〒184-0000 鎌ヶ谷市梶野町 0-0-0 です。';
if(preg_match('/[0-9]{3}-[0-9]{4}/', $str, $data)){
	print $data[0]."<hr>";
}

//練習２
$str = 'お問い合わせは CQW15204@nifty.com まで';
print preg_replace('/[a-z0-9\.\-]+@([a-z0-9\-]+\.)+[a-z0-9\-]+/i', '<a href="mailto:$0">$0</a>', $str);