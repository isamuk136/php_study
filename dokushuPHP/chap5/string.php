<?php
//mb_strlen関数(マルチバイト)
$str = '多田野';
print mb_strlen($str) ."<br>";//結果：3
//strtolower,strtoupper
print strtolower('TDN') ."<br>"; //結果：tdn
print strtoupper('tdn') ."<br>"; //結果：TDN
print ucfirst('tdn') ."<br>"; //結果Tdn
print lcfirst('TDN') ."<br>"; //結果：tDN
print ucwords('tdn baseball player') ."<br>"; //結果：Tdn Baseball Player
echo "<hr>";
//mb_strtolower
print strtolower('ＴＤＮ多田野') ."<br>"; //結果：ＴＤＮ多田野
print mb_strtolower('ＴＤＮ多田野') ."<br>"; //結果：ｔｄｎ多田野
echo "<hr>";
//mb_substr
$str = 'TDN 多田野';
print mb_substr($str, 4,2) ."<br>"; //結果：多田(0から数えて、4文字目から、2文字取り出す)
print mb_substr($str, 4) ."<br>"; //結果：多田野
print mb_substr($str, 4,-2) ."<br>"; //結果：多
print mb_substr($str, -7,3) ."<br>"; //結果：TDN
echo "<hr>";
//str_replace(置換)
$str = 'にわにはにわにわとりがいる';
print str_replace('にわ', 'ニワ', $str, $count) ."<br>"; //結果：ニワにはニワニワとりがいる
print "{$count}箇所、置換<br>"; //結果：３箇所、置換
//配列で置換
$str = array('ジョーイおじさんは素敵','永沢くんは素敵');
$src = array('ジョーイおじさん','永沢くん','素敵');
$rep = array('オッサン','タマネギ星人','デリシャス！！！');
print_r (str_replace($src, $rep, $str, $count));
//結果：Array ( [0] => オッサンはデリシャス！！！ [1] => タマネギ星人はデリシャス！！！ )
print "<br>{$count}箇所、置換<br>"; //4箇所、置換
echo "<hr>";
//explode関数（区切り文字で分割して配列へ）
$str = "多田野と新井と金本と大松とチック"; 
print_r(explode('と',$str)); echo "<br>"; //結果:Array ( [0] => 多田野 [1] => 新井 [2] => 金本 [3] => 大松 [4] => チック )
print_r(explode('や',$str)); echo "<br>";//結果：Array ( [0] => 多田野と新井と金本と大松とチック )
print_r(explode('と',$str, 2)); echo "<br>"; //結果:Array ( [0] => 多田野 [1] => 新井と金本と大松とチック )
print_r(explode('と',$str, -2)); echo "<br>"; //結果:Array ( [0] => 多田野 [1] => 新井 [2] => 金本 ) 
echo "<hr>";
//mb_strpos(特定の文字位置を検索する)
$str = 'にわにはにわにわとりがいる';
print mb_strpos($str, 'にわ') ."<br>"; //結果：0
print mb_strpos($str, 'にわ', 2) ."<br>"; //結果：4
print mb_strpos($str, 'かに') ."<br>"; //結果：(Falseで何も表示されない)
print mb_strrpos($str, 'にわ') ."<br>"; //結果：6(末尾から)
print mb_strrpos($str, 'にわ',-8) ."<br>"; //結果：4
if(mb_strpos($str, 'にわ') !== FALSE){//「!==」である点に注目
	print '文字列が見つかりました'; //
}//結果：文字列が見つかりました
echo "<hr>";
//printf関数（C言語で最初にやったアレだ）(sprintfは文字列を返す)
printf('%2$sは、%3$+0-3.2fということを%1$s<br>','よろしく！','円周率',3.14); //結果：円周率は、+3.14ということをよろしく！
print sprintf('%2$sは、%3$+0-3.2fということを%1$s<br>','よろしく！','円周率',3.14); //結果：（上記と同じ）
echo "<hr>";
//mb_convert_kana（文字列変換、半角から全角など）(第2引数はリファレンスを参照せよ)
$str = 'ＴＤＮﾌﾟﾛｼﾞｪｸﾄ';
print mb_convert_kana($str) ."<br>"; //結果：ＴＤＮプロジェクト(全部全角に変換)
echo "<hr>";
//mb_convert_encoding(文字コード変更)(引数は文字列、変更後、変更前（任意）)
print mb_convert_encoding($str, 'EUC-jp'); //結果：（文字化け）
echo "<hr>";
//mb_convert_variables関数(配列の場合)(引数の並び順が違うから注意)
$name = array('山田','柳田','筒香');
mb_convert_variables('EUC-jp', 'UTF-8,SJIS,JIS', $name);
print_r($name); //結果：（文字化け）
echo "<br><hr>";
