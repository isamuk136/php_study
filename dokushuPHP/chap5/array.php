<?php
//count関数（配列の要素数取得）
$name = array('山田','柳田','筒香');
print count($name) . "<br>"; //結果：3
$data = array(
	array('1','2'),
	array('3','4')
	);
print count($data) . "<br>"; //結果2(二次元配列でも、入れ子の要素数)
print count($data, COUNT_RECURSIVE) ."<br>"; //結果：6（入れ子2 + 二次元目の要素数4）
echo "<hr>";
//array_merge関数（配列の内容を連結）
//補足１：連想配列でキーが重複は後者優先
//補足２：インデックス番号が重複は新たな番号振られるため上書きされない
$arr1 = array(3,3,4);
$arr2 = array(9800,114,514);
$result = array_merge($arr1,$arr2);
print_r($result); echo "<br>";
//結果：Array ( [0] => 3 [1] => 3 [2] => 4 [3] => 9800 [4] => 114 [5] => 514 ) 
$arr1 = array(
	'たまねぎ' => '永沢くん',
	'りんご' => 'アップル'
	);
$arr2 = array(
	'みかん' => 'オレンジ',
	'たまねぎ' => 'オニオン'
	);
$result = array_merge($arr1,$arr2);
print_r($result); echo "<br>";
//結果：Array ( [たまねぎ] => オニオン [りんご] => アップル [みかん] => オレンジ ) 
$result = array_merge_recursive($arr1,$arr2);//要素を上書きしたくない場合
print_r($result); echo "<br><hr>";
//結果：Array ( [たまねぎ] => Array ( [0] => 永沢くん [1] => オニオン ) [りんご] => アップル [みかん] => オレンジ ) 
//implode関数（配列の要素を結合した文字列を返す）
$data = array('浅村','梶谷','山田','柳田','筒香');
print implode('と、', $data) ."は、すげええええ<br><hr>";
//結果：浅村と、梶谷と、山田と、柳田と、筒香は、すげええええ
//配列の先頭と末尾を追加や削除
$data = array('あ','い','う');
print array_push($data,'え','おじさん') ."<br>"; //結果：５（要素数）
print_r($data); echo "<br>";
//結果：Array ( [0] => あ [1] => い [2] => う [3] => え [4] => おじさん )
print array_pop($data) ."<br>"; //結果：おじさん
print_r($data); echo "<br><hr>";
//結果：Array ( [0] => あ [1] => い [2] => う [3] => え )
//array_splice関数（複数追加や削除、配列に直接影響をおよぼす）
$data = array('りんご','みかん','たまねぎ','人間','動物','野球');
print_r(array_splice($data,2,3,array('永沢くん','ジョーイおじさん','犬'))); echo "<br>";
//結果：Array ( [0] => たまねぎ [1] => 人間 [2] => 動物 )
//(↑は書き換える要素の内容)
print_r($data); echo "<br>";
//結果：Array ( [0] => りんご [1] => みかん [2] => 永沢くん [3] => ジョーイおじさん [4] => 犬 [5] => 野球 ) 
//(↑は書き換わった配列の要素全体)
print_r(array_splice($data, -3,-2,array('ナポレオン','こんにちは！'))); echo "<br>";
//結果：Array ( [0] => ジョーイおじさん )
print_r($data); echo "<br>";
//結果：Array ( [0] => りんご [1] => みかん [2] => 永沢くん [3] => ナポレオン [4] => こんにちは！ [5] => 犬 [6] => 野球 ) 
print_r(array_splice($data, 3)); echo "<br>";
//結果：Array ( [0] => ナポレオン [1] => こんにちは！ [2] => 犬 [3] => 野球 ) 
print_r($data); echo "<br>";
//結果：Array ( [0] => りんご [1] => みかん [2] => 永沢くん ) 
print_r(array_splice($data, 2,0,array('バナナ','ブドウ'))); echo "<br>";
//結果:Array()
print_r($data); echo "<br><hr>";
//結果：Array ( [0] => りんご [1] => みかん [2] => バナナ [3] => ブドウ [4] => 永沢くん ) 
//array_slice関数（配列から特定範囲の要素を取得、配列には影響なし）
$data = array('あ','い','う','え','おじさん','か','き','く','け','こ');
print_r(array_slice($data, 2,3)); echo "<br>";
//結果：Array ( [0] => う [1] => え [2] => おじさん ) 
print_r(array_slice($data, 2,3,TRUE)); echo "<br>";
//結果：Array ( [2] => う [3] => え [4] => おじさん ) 
//↑はインデックス番号が引き継がれる
print_r(array_slice($data,5)); echo "<br>";
//結果：Array ( [0] => か [1] => き [2] => く [3] => け [4] => こ ) 
print_r(array_slice($data, -6,-5)); echo "<br><hr>";
//結果：Array ( [0] => おじさん ) 
//sort関数(配列をソートする)
$data = array('Baseball','Apple','DeNA','CC');
sort($data,SORT_STRING);//昇順
print_r($data); echo "<br>";
//結果：Array ( [0] => Apple [1] => Baseball [2] => CC [3] => DeNA ) 
rsort($data,SORT_STRING);//降順
print_r($data); echo "<br>";
//結果：Array ( [0] => DeNA [1] => CC [2] => Baseball [3] => Apple ) 
$data = array(
	'Nagashima' => 3,
	'Oh' => 1,
	'Motoki' => 2,
	'Ochiai' => 60,
	'Matsui' => 55
	);
asort($data, SORT_NUMERIC);
print_r($data); echo "<br>";
//結果：Array ( [Oh] => 1 [Motoki] => 2 [Nagashima] => 3 [Matsui] => 55 [Ochiai] => 60 ) 
ksort($data, SORT_STRING);
print_r($data); echo "<br><hr>";
//結果：Array ( [Matsui] => 55 [Motoki] => 2 [Nagashima] => 3 [Ochiai] => 60 [Oh] => 1 ) 

