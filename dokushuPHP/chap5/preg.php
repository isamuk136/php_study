<?php
//preg_match関数（正規表現）
//補足：一度の実行でひとつの実行結果
$str = '私の電話番号は0399-88-9875、彼のは0398-99-1234で郵便番号は687-1199だよ';
if(preg_match('/([0-9]{2,4})-([0-9]{2,4})-([0-9]{4})/', $str, $data, PREG_OFFSET_CAPTURE)){
	print_r($data); echo "<br><hr>";
}
/*
結果：
Array (
	[0] => Array ( [0] => 0399-88-9875 [1] => 21 )
	[1] => Array ( [0] => 0399 [1] => 21 )
	[2] => Array ( [0] => 88 [1] => 26 )
	[3] => Array ( [0] => 9875 [1] => 29 )
	)
*/
//preg_match_all関数（全てのマッチ文字列を取得）
$str = '私の電話番号は0399-88-9875、彼のは0398-99-1234で郵便番号は687-1199だよ';
if(preg_match_all('/([0-9]{2,4})-([0-9]{2,4})-([0-9]{4})/', $str, $data, PREG_SET_ORDER)){
	//出力結果をマッチ順に取得
	foreach($data as $item){
		print "電話番号：{$item[0]} <br>";
		print "市外局番：{$item[1]} <br>";
		print "市内局番：{$item[2]} <br>";
		print "加入者番号：{$item[3]} <hr>";
	}
}
/*
PREG_SET_ORDERの場合の出力結果
結果：
電話番号：0399-88-9875 
市外局番：0399 
市内局番：88 
加入者番号：9875
--------------------
電話番号：0398-99-1234 
市外局番：0398 
市内局番：99 
加入者番号：1234
*/
//preg_replace関数（正規表現で文字列を置換）
$msg = <<<EOD
サンプルは、「サーバサイド技術の学び舎(http://www.wings.msn.to/)から入手できます。<br>
執筆のノウハウ集「WINGS Knowledge」(HTTP://www31.atwiki.jp/wingsproject)もどうぞ。<br>
EOD;
print preg_replace('|http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?|i', '<a href="$0">$0</a>', $msg) ."<hr>";
//上の第一引数の末尾「i」は大文字小文字区別しない修飾子
//同じく「e」
/*
結果：(カッコ内はリンクになっている)
サンプルは、「サーバサイド技術の学び舎(http://www.wings.msn.to/)から入手できます。
執筆のノウハウ集「WINGS Knowledge」(http://www31.atwiki.jp/wingsproject)もどうぞ。
*/
//preg_split関数（正規表現で文字列を分割）
$today = '2016-01-12';
$result = preg_split('|[/.\-]|', $today);
print "{$result[0]}年{$result[1]}月{$result[2]}日<hr>"; //結果：2o16年01月12日
//PREG_SPLIT_DELIM_CAPTUREオプションを指定してみる
$result = preg_split('|[/.\-]|', $today, -1, PREG_SPLIT_DELIM_CAPTURE);
print_r($result); echo "<hr>";
//結果：Array ( [0] => 2016 [1] => 01 [2] => 12 )
//マルチライン修飾子「m」
//補足：マルチラインにすると「^」は行頭を表すようになる
$str ="334\n9800";
if(preg_match_all('/^[0-9]{1,}/m', $str, $data)){
	foreach($data[0] as $item){
		print "マッチング結果：{$item} <br>";
	}
}
echo "<hr>";
/*
結果：
マッチング結果：334 
マッチング結果：9800 
*/
//「e」修飾子で置き換え後の結果を引数に関数で利用してみる
$msg = <<<EOD
サンプルは、「サーバサイド技術の学び舎(http://www.wings.msn.to/)から入手できます。<br>
執筆のノウハウ集「WINGS Knowledge」(HTTP://www31.atwiki.jp/wingsproject)もどうぞ。<br>
EOD;
print preg_replace('|http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?|ie', "strtoupper('$0')", $msg) ."<hr>";
/*
結果：
サンプルは、「サーバサイド技術の学び舎(HTTP://WWW.WINGS.MSN.TO/)から入手できます。
執筆のノウハウ集「WINGS Knowledge」(HTTP://WWW31.ATWIKI.JP/WINGSPROJECT)もどうぞ。
*/