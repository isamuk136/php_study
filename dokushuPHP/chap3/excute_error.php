<?php
//実行演算子
$result = `dir` ;
print mb_convert_encoding($result,'UTF-8','sjis'); //結果：環境によるよ
//エラー制御演算子
print 1/0; //結果：エラーを吐く
print "<br>";
@print 1/0; //何も表示されない