<?php
//インクリメントの前後の違い
$x = 10;
$y = ++$x;
print $x . "<br>"; //結果：11
print $y . "<br>"; //結果：11

$x = 10;
$y = $x++;
print $x . "<br>"; //結果：11
print $y . "<br>"; //結果：10