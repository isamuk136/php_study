<?php
//文字列混在の比較(==)
var_dump('3.14' == 3.14000); //結果1： true
var_dump('3.14E2' == 314); //結果2： true
var_dump('0x10' == 16); //結果3： true
var_dump('010' == 8); //結果4： false(8進数)
var_dump('1.3abc' == 1.3); //結果5： true
var_dump('abc' == 0); //結果6： true
var_dump('3.14' == '3.1400000'); //結果7： true 
var_dump('3.14E2' == '314' ); //結果8： true
var_dump('1.3abc' == '1.3'); //結果： false
print "<br>";
//(===)の場合
var_dump('3.14E2' === 314 ); //結果： false
var_dump('x' === 0); //結果： false
var_dump('1' === 1); //結果： false




