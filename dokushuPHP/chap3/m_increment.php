<?php
//文字列をインクリメントしてみる
$i = 'a';
print ++$i . "<br>"; //結果：b
$i = 'z';
print ++$i . "<br>"; //結果：aa
$i = 'aa';
print ++$i . "<br>"; //結果：ab
$i = 'a1';
print ++$i . "<br>"; //結果：a2