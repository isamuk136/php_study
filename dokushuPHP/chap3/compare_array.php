<?php
//配列の比較
$data1 = array(1,2,3);
$data2 = array(1,5);
var_dump($data1 < $data2); //結果； false
$data1 = array(1,2,3);
$data2 = array(1,5,1);
var_dump($data1 < $data2); //結果； true
$data1 = array(1,2,3);
$data2 = array(1,2,'3');
var_dump($data1 == $data2); //結果； true
var_dump($data1 === $data2); //結果： false
$data1 = array('a' => 'A','b' => 'B');
$data2 = array('b' => 'B','a' => 'A');
var_dump($data1 == $data2); //結果； true
var_dump($data1 === $data2); //結果： false