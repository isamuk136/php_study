<!-- 未完成 -->
<?php
require_once 'Auth/Auth.php';

//認証フォーム呼び出しの為のユーザー定義関数
function myLogin($usr,$status){
	//エラーメッセージ候補を連想配列で用意
	$errs = array(
		AUTH_IDLED => 'アイドル時間を超えた。再ログインを頼む',
		AUTH_EXPIRED => '期限が切れた。再ログインを頼む',
		AUTH_WRONG_LOGIN => 'ユーザー/パスワードが間違っている'
	);
	//認証フォームを呼び出し
	require_once 'login.php';
}

//Authクラスのインスタンス化
$auth = new Auth('MDB2',
	array(
		'dsn' => 'mysqli://selfusr:selfpass@127.0.0.1/selfphp', 
		//↑は環境に合わせて設定する必要あり。現状エラーを吐くが後回し
		'table' => 'usr',
		'usernamecol' => 'uid',
		'passwordcol' => 'passwd',
		'db_fields' => '*'
	),'myLogin'
);

//認証処理の実行
$auth->start();
//認証の成否を判定（未認証、認証失敗時にはスクリプトを終了）
if(!$auth->checkAuth()){die();}