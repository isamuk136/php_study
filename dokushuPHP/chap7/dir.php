<!-- DirectoryIteratorクラス -->
<!-- 補足：指定されたディレクトリ配下のファイル情報にアクセスするためのクラス -->
<table border="1">
<tr>
<th>ファイル</th><th>サイズ</th><th>最終アクセス日</th><th>最終更新日</th>
</tr>
<?php
//カレントフォルダをオープン
$dir = new DirectoryIterator('./');
//フォルダ内容を順番に読み込み
foreach($dir as $file){
	//読み込んだ要素がファイルである（サブフォルダでない）場合のみ表示処理
	if($file->isFile()){
?>
<tr>
	<!-- 単発処理なので、date関数を使って処理（DateTimeクラスでもできる） -->
	<td><?php print mb_convert_encoding($file->getFileName(), 'UTF-8', 'SJIS-WIN'); ?></td>
	<td><?php print $file->getSize(); ?>B</td>
	<td><?php print date('Y/m/d H:i:s',$file->getAtime()) ?></td>
	<td><?php print date('Y/m/d H:i:s',$file->getMtime()) ?></td>
</tr>
<?php
	}
}
?>
</table>
<!--
結果：
（結果は環境によるよねｗｗｗ）
-->