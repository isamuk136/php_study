<?php
//DateTimeクラスで色々やってみる
//現在の日付を呼び出す
$now = new DateTime();
print $now -> format('Y年の、m月で、d日なんだよなぁ、時刻は、H:i:s')."<br>";
// 結果：2016年の、01月で、28日なんだよなぁ、時刻は、14:50:43（実行した時間によって変わる）
$doraemon = new DateTime('2112/9/12');
print $doraemon -> format('Y-m-d')."<br>";
//結果：2112-09-12
$time = new DateTime();
$time -> setDate(1989,5,31);
$time -> setTime(0,96);//60分は1時間とみなされる
print $time -> format('Y-m-d H:i')."<br>";
//結果：1989-05-31 01:36
//Unixタイムスタンプ
$now = new DateTime();
$now -> setTimestamp(time());
print $now -> format('西暦Y年、日付不明、時刻は、H時くらい')."<hr>";
//結果：西暦2016年、日付不明、時刻は、15時くらい(実行した時間によって変わる)
//formatメソッドで色々
$now = new Datetime();
print $now -> format("g時だよ、aのね。今は、m月で、今月の日数はt日ある。")."<br>";
//結果：3時だよ、pmのね。今は、01月で、今月の日数は31日ある。(実行日時によって変わる)
print $now ->format('L') ? "うるう年だよ<br>":"うるう年じゃないよ<br>";
//結果：うるう年だよ
print $now -> format(DateTime::RSS)."<hr>";//現在時刻を静的定数を呼び出す
//結果：Thu, 28 Jan 2016 15:39:01 +0900（実行日時によって変わる）
//createFromFormat静的メソッドを使ってみる
$fmt = 'Y年なり。m月なり。d日なりけりｗｗｗ';
$time ='1989年なり。5月なり。31日なりけりｗｗｗ';
$isamu = DateTime::createFromFormat($fmt,$time);
print $isamu->format('Y-m-d')."<hr>";
//結果：1989-05-31
//addとsubメソッド使って加算や減算してみるよ
$now = new DateTime();
print $now->format('今は、H時i分ですよ。')."<br>";
//結果：今は、15時51分ですよ。(実行日時によって変わる)
$now->add(new DateInterval('P0YT1H36M'));
print "1時間36分後は、". $now->format('今は、H時i分ですよ。')."<hr>";
//diffメソッドで時刻値の差分を取得
$isamu = new DateTime('1989/5/31');
$now = new DateTime();
$interval = $isamu->diff($now,TRUE);
print "生まれて、". $interval->format('%Y年%Mヵ月%D日')."、たちました<hr>";
//結果：生まれて、26年07ヵ月28日、たちました（実行日時によって変わる）
//日付、時刻の関数をみてみる
//checkdate関数使って、カレンダーを表示
function calendar($year,$month){
	for($i=1;$i<32;$i++){
		if(checkdate($month,$i,$year)){
			print "{$i} ";
		}
	}
	print "<br>";
}
print "2016年2月のカレンダー<br>";
calendar(2016,2); echo"<hr>";
/*
結果：2016年2月のカレンダー
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29
*/
//練習問題7.2
$fmt = 'Y年m月d日';
$time = '2010年12月04日';
$dt = DateTime::createFromFormat($fmt,$time);
print $dt->format('Y/m/d(D)')."<hr>";