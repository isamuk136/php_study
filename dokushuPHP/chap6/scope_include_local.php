<?php
//関数でインクルードしてローカル変数として呼ぶ
function checkScope(){
	require_once 'scope_included.php';
	return $scope;
}
print checkScope() ."<br>";//結果：ジョーイおじさん「アクセスできただけえ(ﾄﾞﾔｯ)」
// print $scope ."<br>";//結果：NULL
