<?php
//ユーザー定義関数にディフォルトの引数を設定
function getTriangleArea($base=5,$height=1){
	return ($base * $height) /2;
}
$area=getTriangleArea();
print "面積は{$area}です<br>";//結果：面積は2.5です
$area=getTriangleArea(10);
print "面積は{$area}です<br>";//結果：面積は5です
$area=getTriangleArea(10,6.68);
print "面積は{$area}です<br>";//結果：面積33.4です