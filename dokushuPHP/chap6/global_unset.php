<?php
//関数内でグローバル変数をunsetして挙動を確認
$num = 10;
function checkScope(){
	global $x;
	unset($x);
	return ++$x;
}
@print checkScope() ."<br>";//結果：1
print $num ."<br>";//結果：10
// 上記のようにグローバル変数を破棄できない
// 関数内でグローバル変数を破棄したい場合
// 補足：$GLOBALという連想配列（すべてのグローバル変数を管理する連想配列）を使う
