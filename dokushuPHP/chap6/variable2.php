<?php
//名前付き引数と可変長引数を混在させる例
function replaceContents($path){
	$data = file_get_contents($path);
	// ↑指定されたパスからファイルを読み込み
	for($i=1;$i<func_num_args();$i++){
		$data = str_replace('{'.($i-1).'}', func_get_arg($i), $data);
	}
	return $data;
}
print replaceContents('data.dat','永沢くん','君は卑怯だね');
/*
結果：
名前： 永沢くん 様

一言： 君は卑怯だね
*/