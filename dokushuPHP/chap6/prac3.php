<?php
function processNumber($func){
	for($i=1;$i<func_num_args();$i++){
		$result[] = $func(func_get_arg($i));
	}
	return $result;
}

list($x,$y,$a,$b,$c) = processNumber(
	function($num){return $num *$num;},
	5,15,2,3,4);
print "{$x},{$y},{$a},{$b},{$c}<br>"; //結果：25,225,4,9,16