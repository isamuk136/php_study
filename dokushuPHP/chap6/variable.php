<?php
//可変長引数の関数
function sum(){
	$result = 0;//結果を格納するための変数
	$args = func_get_args();//引数を配列に格納
	foreach ($args as $value) {
		$result += $value;
	}
	return $result;
}
print sum(33,0.4) ."<br>";//結果：33.4
print sum(1000,2000,3000,3800) ."<hr>";//結果：9800
//上記を別の形で書き換え
function sum2(){
	$result = 0;
	for($i=0;$i<func_num_args();$i++){
		$result += func_get_arg($i);
	}
	return $result;
}
print sum2(114000,514) ."<br>";//結果：114514
print sum2(10,20,30,36.3) ."<hr>";//結果：96.3
