<?php
//変数のスコープの確認
$x = 10;
function checkScope(){
	$x = 0;
	return ++$x;
}
print checkScope() ."<br>";//結果：1
print $x ."<br>";//結果:10
//関数内でグローバル変数を操作する
$i = 10;
function checkGlobal(){
	global $i;
	return ++$i;
}
print checkGlobal() ."<br>";//結果：11
print $i ."<br>";//結果：11

