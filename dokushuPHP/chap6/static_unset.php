<?php
//静的変数
//補足：関数のの中で宣言、関数終了でも維持される、
//補足：初回だけ初期化、あくまでローカル変数)
function checkStatic(){
	static $num = 0;
	return ++$num;
}
//print $num ."<br>";//結果：NULL
for($i=0;$i<10;$i++){
	print checkStatic() .",";
}//結果：1,2,3,4,5,6,7,8,9,10,

//static変数をunsetした時の動作確認
function checkStatic(){
	static $x = 0;
	$x++;
	print "unset前:{$x}<br>";
	unset($x);
	$x = 13;
	print "unset後：{$x}<hr>";
}
for($i=0;$i<3;$i++){
	checkStatic();
}
/*
結果：
unset前:1
unset後：13
----------
unset前:2
unset後：13
----------
unset前:3
unset後：13
*/