<?php
//引数の値渡しと参照渡し
//値渡しの例
function increment($num){
	$num++;
	return $num;
}
$value = 10;
print increment($value) ."<br>";//結果：11
print $value ."<hr>";//結果：10
//参照渡しの例
function increment2(&$num){
	$num++;
	return $num;
}
$value2 = 10;
print increment2($value2) ."<br>";//結果：11
print $value2 ."<hr>";//結果：10
