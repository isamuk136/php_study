<?php
//高階関数の実装
//高階関数my_array_walkを定義
function my_array_walk($array,$func){
	//配列$arrayの内容を順に処理
	foreach($array as $key => $value){
		$func($value,$key); // $funcで指定された関数を呼び出し
	}
}
//配列を処理するためのユーザー定義関数
function showItem($value,$key){
	print "{$key}：{$value}<br>";
}
$data = array(
			'ヤクルト' => '山田',
			'巨人' => '坂本',
			'阪神' => '鳥谷',
			'広島' => '菊池',
			'中日' => '福田',
			'横浜' => '梶谷'
			);
my_array_walk($data,'showItem');
/*
結果：
ヤクルト：山田
巨人：坂本
阪神：鳥谷
広島：菊池
中日：福田
横浜：梶谷
*/
echo "<hr>";
$result = 0; //結果を格納するグローバル変数
function sum($value,$key){
	global $result;//グローバル変数を定義
	$result += $value;//配列の値を$resultに加算
}
$data2 = array(10.1,10.0,10.1,3.2);
my_array_walk($data2,'sum');
print "合計値：{$result}<hr>";//結果：合計値：33.4
