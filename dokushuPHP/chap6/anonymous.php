<?php
//引数に無名関数を使う
function my_array_walk($array,$func){
	foreach($array as $key => $value){
		$func($value,$key);
	}
}
$data = array(
			'ヤクルト' => '山田',
			'巨人' => '坂本',
			'阪神' => '鳥谷',
			'広島' => '菊池',
			'中日' => '福田',
			'横浜' => '梶谷'
			);
my_array_walk($data,
	function($value,$key){
		print "チーム名：{$key}、選手名：{$value}<br>";
	}
);
/*
結果：
チーム名：ヤクルト、選手名：山田
チーム名：巨人、選手名：坂本
チーム名：阪神、選手名：鳥谷
チーム名：広島、選手名：菊池
チーム名：中日、選手名：福田
チーム名：横浜、選手名：梶谷
*/