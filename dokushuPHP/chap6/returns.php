<?php
function max_min(){
	$args = func_get_args();
	return array(max($args),min($args));
}
$result = max_min(4,8,12,16,20,24,28,33);
print_r($result); echo "<br>";
//結果：Array ( [0] => 33 [1] => 4 )
//下はlist命令を使って受け取る
list($max,$min) = max_min(4,8,12,16,20,24,28,33);
print "最大値：{$max}、最小値：{$min}<br>";
//結果：最大値：33、最小値：4