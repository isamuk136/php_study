<!-- リクエストヘッダを取得してテーブル形式で出力する -->
<?php require_once '../../Encoding.php'; ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- この下の空行にタイトルを記入 -->
<title>
ヘッダ情報
</title>
</head>
<body>
<!-- ここから本文を記入 -->
<table border="1">
<?php
// $_SERVERのキーと値を順に取得
foreach($_SERVER as $key => $value){
	//キーが「HTTP_」で始まる場合のみ(開始位置が「0」と完全に等しい時)
	if((mb_strpos($key,'HTTP_')===0)){
?>
	<tr>
		<th><?php e($key); ?></th>
		<td><?php e($value); ?></td>
	</tr>
<?php
	}
}
?>
</table>
</body>
</html>