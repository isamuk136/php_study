<?php
//セッションを明示的に破棄する
session_start();

//セッション変数を空にする
$_SESSION = array();

//セッションクッキー（ID受け渡しのためのクッキー）を破棄する
if(isset($_COOKIE[session_name()])){
	setcookie(session_name(),'',time()-3600,'/');
}

//セッションを破棄
session_destroy();

echo "セッション削除したよ～";