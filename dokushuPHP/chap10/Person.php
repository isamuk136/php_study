<?php
 class Person{
	//プロパティ
	public $name;
	public $age;

	//コンストラクタ
	public function __construct($name,$age){
		$this->name = $name;
		$this->age = $age;
	}

	//デストラクタ
	public function __destruct(){
		print '<p>'. __CLASS__. 'オブジェクトが破棄されたで～</p>';
	}

	//メソッド
	public function show(){
		print "名前は、{$this->name}で、年齢は、{$this->age}歳です。<br>";
	}
}