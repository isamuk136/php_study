<?php
require_once 'Figure.php';

class Square extends Figure{//四角形クラス、図形クラスを継承
	//四角形の面積を求めるgetAreaメソッド
	public function getArea(){
		return $this->width * $this->height;
	}
}