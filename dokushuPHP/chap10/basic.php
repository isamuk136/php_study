<?php
require_once 'Person.php';
require_once 'Staticing.php';

// $p = new Person();
// $p->name = '永沢くん';
// $p->age = 9;
// print "名前は、{$p->name}で、年齢は、{$p->age}歳です。<br>";
// $p->show();
// echo "<hr>";

$p2 = new Person('イサム',136);
$p2->show();
echo "<hr>";

//静的プロパティと、クラス定数、静的メソッドをそれぞれ呼び出す
print Staticing::$num ."<br>";
print Staticing::TDN ."<br>";
print Staticing::nandeya(0.1) ."<br>";
echo "<hr>";

