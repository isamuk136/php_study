<?php
require_once 'SuperTamanegiPerson.php';
require_once 'MiniTamanegiPerson.php';
require_once 'ShibakariPerson.php';

$stp = new SuperTamanegiPerson("永沢くん",9);
$stp->show();
$stp->tamaneging();
echo "<hr>";

$mtp = new MiniTamanegiPerson("永沢太郎（弟）",0);
$mtp->show();
$mtp->tamaneging();
echo "<hr>";

$shipa = new ShibakariPerson("ジョーイおじさん",29,9800);
$shipa->show();
echo "<hr>";
