<?php
class Figure{//図形クラス
	//プロパティ（protectedで制限）
	protected $width;//幅
	protected $height;//高さ

	//コンストラクタ
	public function __construct($width,$height){
		$this->width = $width;
		$this->height = $height;
	}
}