<?php
require_once 'Figure.php';

class Triangle extends Figure{//三角形クラス、図形クラスを継承
	//三角形の面積を求めるgetAreaメソッド
	public function getArea(){
		return $this->width * $this->height /2;
	}
}