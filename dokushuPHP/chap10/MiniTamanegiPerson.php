<?php
require_once 'TamanegiPerson.php';

class MiniTamanegiPerson extends TamanegiPerson{
	//TamanegiPersonクラスのtamanegingメソッドをオーバーライド
	public function tamaneging(){
		parent::tamaneging(); //スーパークラスのtamanegingメソッド
		print 'ただし、小さすぎて効果は薄い<br>';
	}
}