<?php
require_once 'Person.php';

class ShibakariPerson extends Person{//芝刈り人間クラス
	//追加プロパティ
	public $sp = 0;//芝刈りポイント、初期値0

	//スーパークラスのコンストラクタをオーバーライド
	public function __construct($name,$age,$sp){
		//スーパークラスのコンストラクタを呼び出し
		parent::__construct($name,$age);
		//独自のspプロパティも初期化
		$this->sp = $sp;
	}

	//spプロパティ対応にshowメソッドもオーバーライド
	public function show(){
		print "私は芝刈り人間です<br>";
		parent::show();
		print "芝刈りポイント：{$this->sp}";
	}
}