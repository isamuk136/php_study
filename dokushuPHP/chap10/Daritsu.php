<?php
class Daritsu{
	//プロパティはprivate権限
	public $dasuu;//打数
	public $anda;//安打
	//コンストラクタ
	public function __construct(){
		//打数の初期値を1,安打の初期値を0とする
		$this->dasuu = 1;//
		$this->anda = 0;
	}
	//ゲッターとセッター
	//ゲッター
	public function getDasuu(){
		return $this->dasuu;
	}
	public function getAnda(){
		return $this->anda;
	}
	//セッター
	public function setDasuu($dasuu){
		//打数が数字で正の数の時だけ実行
		if(is_numeric($dasuu) && $dasuu > 0){
			$this->dasuu = $dasuu;
		}
	}
	public function setAnda($anda){
		//安打が数字で正の数の時だけ実行
		if(is_numeric($anda) && $anda > 0){
			$this->anda = $anda;
		}
	}

	//メソッド
	public function daritsu(){
		return round($this->anda / $this->dasuu ,3);
	}
}

//Daritsuクラスを使う
$d = new Daritsu();
$d->setDasuu(14);
$d->setAnda(4);
print "打数：".$d->getDasuu() ."<br>";
print "安打：".$d->getAnda() ."<br>";
print "打率：".$d->daritsu() ."<br>";