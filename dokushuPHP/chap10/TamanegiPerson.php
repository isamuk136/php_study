<?php
//継承元のクラスをインポート
require_once 'Person.php';

class TamanegiPerson extends Person{
	//サブクラス独自のメソッドを定義
	public function tamaneging(){
		print "{$this->name}は、タマネギングしてます！<br>";
	}
}