<?php
//switch文
$rank = '甲';
switch ($rank) {
	case '甲':
		print '大変良い！';
		break;
	case '乙':
		print '良い！';
		break;
	case '丙':
		print "悪い！";
		break;
	default:
		print '???';
		break;
}//結果：大変良い！
print "<br>";
//break文を意図的に省略した例
$team = '阪神';
switch ($team) {
	case 'ヤクルト':
	case '巨人':
	case '阪神':
	case '広島':
	case '中日':
	case '横浜':
		print $team . 'はセリーグです';
		break;
	case 'ソフトバンク':
	case '日本ハム':
	case 'ロッテ':
	case '西武':
	case 'オリックス':
	case '楽天':
		print $team . 'はパリーグです';
		break;
}//結果：阪神はセリーグです
print "<br>";
//switchは「===」ではなく、「==」で比較されるのがわかる例
$str = '筒香';
switch ($str) {
	case 0:
		print '値は「0」です';
		break;
	case '筒香':
		print '値は「筒香」です';
		break;
}//結果：値は「0」です
