<?php
//continue文(カウントして3の倍数だけ抜かす)
for($i=1;$i<=10;$i++){
	if($i % 3 == 0){continue;}
	echo $i . ",";
}// 結果：1,2,4,5,7,8,10,
echo "<hr>";
//break使って、答えが40を超えない九九表
for($a=1;$a<=9;$a++){
	for($b=1;$b<=9;$b++){
		$result = $a * $b;
		if($result > 40){break;} //ここを「break 2;」にしたら、二重ループをまとめて抜ける
		echo $result . " ";
	}
	echo "<br>";
}
/*結果：
1 2 3 4 5 6 7 8 9 
2 4 6 8 10 12 14 16 18 
3 6 9 12 15 18 21 24 27 
4 8 12 16 20 24 28 32 36 
5 10 15 20 25 30 35 40 
6 12 18 24 30 36 
7 14 21 28 35 
8 16 24 32 40 
9 18 27 36 */
echo "<hr>";
//上記をgoto文で40に最初になった時点で終了させる
for($a=1;$a<=9;$a++){
	for($b=1;$b<=9;$b++){
		$result = $a * $b;
		if($result > 40){goto end;} //ここを変更
		echo $result . " ";
	}
	echo "<br>";
}
end : //ラベル
/*結果:
1 2 3 4 5 6 7 8 9 
2 4 6 8 10 12 14 16 18 
3 6 9 12 15 18 21 24 27 
4 8 12 16 20 24 28 32 36 
5 10 15 20 25 30 35 40*/
echo "<hr>";
//switchでcontinue使うときの注意
for($i=1;$i<=5;$i++){
	$result = 0;
	switch ($i) {
		case 1:
		case 2:
		case 3:
		case 5:
			$result = $i *$i;
			break;
		case 4:
			continue 2;
	}
	print "{$i} の2乗は {$result} です<br>";
}
/*結果：
1 の2乗は 1 です
2 の2乗は 4 です
3 の2乗は 9 です
5 の2乗は 25 です*/