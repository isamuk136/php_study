<?php
//foreach文（普通の配列の場合）
$name = array('ジョーイおじさん','永沢くん','筒香');
foreach($name as $value){
	print "名前は「{$value}」<br>";
}
/*結果：
名前は「ジョーイおじさん」
名前は「永沢くん」
名前は「筒香」*/
echo "<hr>";
//foreach文（連想配列の場合）
$team = array(
	'巨人' => '関東',
	'阪神' => '関西',
	'楽天' => '東北'
	);
foreach ($team as $key => $value) {
	print "「{$key}」は「{$value}」のチーム<br>";
}
/*結果：
「巨人」は「関東」のチーム
「阪神」は「関西」のチーム
「楽天」は「東北」のチーム*/
echo "<hr>";
//値変数の参照渡しをして、配列の内容を書き換えるforeach文
$name = array('ジョーイおじさん','永沢くん','筒香');
foreach($name as &$value){ // 「&」を除くと、値渡しになり、元の配列は変更されない
	$value = 'New' . $value;
}
print_r($name);
// 結果：Array ( [0] => Newジョーイおじさん [1] => New永沢くん [2] => New筒香 )