<?php
//キャスト
var_dump((int)1.5);
var_dump((int)true);
var_dump((string)false);
var_dump((array)'ジョーイおじさん');
var_dump((unset)1.5);
/*結果：
int(1) int(1) string(0) "" array(1) { [0]=> string(24) "ジョーイおじさん" } NULL
*/

